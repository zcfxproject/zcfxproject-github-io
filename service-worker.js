/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/3.6.3/workbox-sw.js");

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "404.html",
    "revision": "1971b1703a9751fefaeba3a2656ed805"
  },
  {
    "url": "assets/css/0.styles.91ee64f2.css",
    "revision": "a96baaedc98d99dacf2774f5ae665970"
  },
  {
    "url": "assets/img/search.83621669.svg",
    "revision": "83621669651b9a3d4bf64d1a670ad856"
  },
  {
    "url": "assets/js/1.a15b83f1.js",
    "revision": "5041fe39eb293447cd4e42ff84b5f248"
  },
  {
    "url": "assets/js/10.fd8dd766.js",
    "revision": "66da5ff0e040fb7589a892a9226844dd"
  },
  {
    "url": "assets/js/11.5c083cb1.js",
    "revision": "2361c9d3a9d99505bbadaabfe167452d"
  },
  {
    "url": "assets/js/12.bb0986df.js",
    "revision": "c8e8a7698fbf847246ed64b4c6315d5b"
  },
  {
    "url": "assets/js/13.43b92b66.js",
    "revision": "68322708f27a1e146f2f61f1c968e3d2"
  },
  {
    "url": "assets/js/14.60e57a1d.js",
    "revision": "87f53fb828103e1cb37c6bb76b0e4580"
  },
  {
    "url": "assets/js/15.1619be8a.js",
    "revision": "ac5cf15a5c01c89d9f3eb0151ddbe169"
  },
  {
    "url": "assets/js/16.9df0004e.js",
    "revision": "79d6310a843d8479341be6dd04554a2e"
  },
  {
    "url": "assets/js/17.4a8ba7ba.js",
    "revision": "a2359f735ddcbcd7aecd49e9b5fb1baf"
  },
  {
    "url": "assets/js/18.20f0c6e0.js",
    "revision": "f5ab47e1311752a06e2465b8c03eece0"
  },
  {
    "url": "assets/js/3.e9d2ec11.js",
    "revision": "de08963251fc72aa59c3247b4b989b59"
  },
  {
    "url": "assets/js/4.085bbc07.js",
    "revision": "d4a2c4764d1df17abab89c2e1913bb87"
  },
  {
    "url": "assets/js/5.3225ba39.js",
    "revision": "264933fda70c5d4bd12160918bf4d68c"
  },
  {
    "url": "assets/js/6.3ee8f9a0.js",
    "revision": "a51d43131ceb76bebbe198a316af5a33"
  },
  {
    "url": "assets/js/7.6455e94a.js",
    "revision": "3cfbf9c6c5f631dd76bf30b6dd42d527"
  },
  {
    "url": "assets/js/8.c1df1cfc.js",
    "revision": "7c554c78bc7f8eb129aa816bc6d01bb2"
  },
  {
    "url": "assets/js/9.5590d66c.js",
    "revision": "da47f2bf233431f1e533d34d8fa05437"
  },
  {
    "url": "assets/js/app.d13eff23.js",
    "revision": "8ca12bae03da964a935f9c90faa9f658"
  },
  {
    "url": "ava.png",
    "revision": "15b1c81af357d7bf9ad3b0d7ae0e70ae"
  },
  {
    "url": "icons/email.svg",
    "revision": "c0482baf06ff13dfee55383a0646f373"
  },
  {
    "url": "icons/facebook.svg",
    "revision": "a937dc2aa703eb4ab7ef54c334a0521d"
  },
  {
    "url": "icons/github.svg",
    "revision": "b53ff55e09a092f9f0cbbf74af01b715"
  },
  {
    "url": "icons/instagram.svg",
    "revision": "622e2947df6ed5a12b06a6ea7cb70777"
  },
  {
    "url": "icons/linkedin.svg",
    "revision": "c1eb27404bbc0d6052620ac79194ae19"
  },
  {
    "url": "icons/twitter.svg",
    "revision": "7a4d9f0fe157437d3258bbc3b785066d"
  },
  {
    "url": "icons/youtube.svg",
    "revision": "f4d46a74f2230eb4b0a079b6c764bac6"
  },
  {
    "url": "index.html",
    "revision": "7cedf9ff8ca69a4e1974a47daf4fbddd"
  },
  {
    "url": "logo.png",
    "revision": "cf32e956e3fe2e870c3b3453eb8d4628"
  },
  {
    "url": "t3-admin-img/router.png",
    "revision": "2785e36b7d885000d3c0217ca6b0f830"
  },
  {
    "url": "t3-admin/apirequest.html",
    "revision": "7063e19b3e192656bff36d2bd35c4f0f"
  },
  {
    "url": "t3-admin/authconfig.html",
    "revision": "8af8fdae7955614ea3b53115910d81b6"
  },
  {
    "url": "t3-admin/compnent.html",
    "revision": "384ef4446fcc0cac99322b3ff75fe935"
  },
  {
    "url": "t3-admin/globalconfig.html",
    "revision": "d7cbaf43b35ed5e5a6393cfe1e8244ac"
  },
  {
    "url": "t3-admin/globaldirect.html",
    "revision": "d79c21ee30360cfd8ff6a7fa435cf163"
  },
  {
    "url": "t3-admin/index.html",
    "revision": "d11e017b4cb653fd68768ef6e3effdf5"
  },
  {
    "url": "t3-admin/mock.html",
    "revision": "11c230bae5f166b9045544c6059de835"
  },
  {
    "url": "t3-admin/routerconfig.html",
    "revision": "c0943a64e4820b3a4e4741fbebe8adc9"
  },
  {
    "url": "t3-admin/start.html",
    "revision": "e176fc75ef2e20d8ce42da136de9a9ef"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.suppressWarnings();
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
addEventListener('message', event => {
  const replyPort = event.ports[0]
  const message = event.data
  if (replyPort && message && message.type === 'skip-waiting') {
    event.waitUntil(
      self.skipWaiting().then(
        () => replyPort.postMessage({ error: null }),
        error => replyPort.postMessage({ error })
      )
    )
  }
})
